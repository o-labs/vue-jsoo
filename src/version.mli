(** vue version *)
type t = [ `v2 | `v3 ]

(** set global version **)
val v : t ref
val set : t -> unit
