open Ppxlib
open Ast_builder.Default

let app : (string * [`root | `component] * string) option ref = ref None

type options = {
  id: string option;
  name: string option;
  rm_prefix: bool;
  template: expression option;
  render: expression option;
  props: string list option;
  computed: string option;
  methods: string option;
  watch: string option;
  debug: bool;
}

let empty_options = {
  id=None; name=None; rm_prefix=true; template=None; render=None; props=None;
  computed=None; methods=None; watch=None; debug=false }

let rec parse_string_list f acc e = match e.pexp_desc with
  | Pexp_construct ({txt=Lident "::";_}, Some {pexp_desc=Pexp_tuple [ a; b ]; _}) ->
    begin match f a with
      | None -> failwith "unexpected expression in list"
      | Some r -> parse_string_list f (r :: acc) b
    end
  | Pexp_construct ({txt=Lident "[]";_}, None) -> List.rev acc
  | _ -> failwith "unexpected expression in list"

let get_module_options l =
  let aux l = List.fold_left (fun acc ({txt; _}, e) ->
      let n = Longident.name txt in
      match n, e.pexp_desc with
      | "debug", _ -> {acc with debug=true}
      | "id", Pexp_constant (Pconst_string (s, _, _)) -> { acc with id=Some s }
      | "name", Pexp_constant (Pconst_string (s, _, _)) -> { acc with name=Some s }
      | "remove_prefix", Pexp_construct ({txt=Lident "false"; _}, None) -> { acc with rm_prefix=false }
      | "template", _ -> { acc with template = Some e }
      | "render", _ -> { acc with render = Some e }
      | "props", _ ->
        let props = parse_string_list
            (function {pexp_desc=Pexp_constant Pconst_string (s, _, _); _} -> Some s | _ -> None)
            [] e in
        { acc with props = Some props }
      | "computed", Pexp_ident {txt=Lident s; _} -> { acc with computed = Some s }
      | "methods", Pexp_ident {txt=Lident s; _} -> { acc with methods = Some s }
      | "watch", Pexp_ident {txt=Lident s; _} -> { acc with watch = Some s }
      | _ -> acc) empty_options l in
  List.find_map (function
      | {attr_name={txt="vue"; _}; attr_payload = PStr [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_record (l, _); _}, _) ; _} ]; _} ->
        Some (aux l)
      | {attr_name={txt="vue"; _}; _} -> Some empty_options
      | _ -> None) l

let derive_module ~loc ~options t =
  let open Ppx_deriving_jsoo_lib in
  let open Common in
  let id = Option.value ~default:"app" options.id in
  let options_deriving =  {
    default_options with f_prop=`mutable_; g_rm_prefix=`bool options.rm_prefix} in
  let str_jsoo = Ppx_deriving_jsoo_lib.Deriver_exe.process ~loc ~rec_flag:Nonrecursive
      ~options:options_deriving ~recursive:None [ t ] in
  let all_ctype = match options.computed, options.methods, options.watch with
    | None, None, None -> pcty_constr ~loc (Located.lident ~loc "data") []
    | _ ->
      let fields = List.filter_map (function
          | None -> None
          | Some s -> Some (pctf_inherit ~loc (pcty_constr ~loc (Located.lident ~loc s) [])))
          [ Some "data"; options.computed; options.methods; options.watch ] in
      pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields in
  let jsoo_type = if t.ptype_name.txt = "t" then "jsoo" else t.ptype_name.txt ^ "_jsoo" in
  let str_ctype = [
    pstr_class_type ~loc [
      class_infos ~loc ~virt:Concrete ~params:[] ~name:{txt="data";loc}
        ~expr:(pcty_constr ~loc (Located.lident ~loc jsoo_type) []);
    ];
    pstr_class_type ~loc [
      class_infos ~loc ~virt:Concrete ~params:[] ~name:{txt="all";loc}
        ~expr:all_ctype;
    ]
  ] in
  let component = match options.template, options.render with
    | None, None -> None
    | Some tpl, _ -> Some (`template tpl)
    | _, Some rd -> Some (`render rd) in
  let str_more, mod_apply, kind = match component with
    | None -> [
        pstr_value ~loc Nonrecursive [
          value_binding ~loc ~pat:(pvar ~loc "id") ~expr:(estring ~loc id)
        ] ], "Vue_js.Make", `root
    | Some x ->
      let element = match x with
        | `template tpl -> [%expr Vue_component.CTemplate [%e tpl] ]
        | `render rd -> [%expr Vue_component.CRender [%e rd] ] in
      let props = match options.props with
        | None -> [%expr None]
        | Some l -> [%expr Some (PrsArray [%e elist ~loc @@ List.map (estring ~loc) l])] in [
        pstr_value ~loc Nonrecursive [
          value_binding ~loc ~pat:(pvar ~loc "name") ~expr:(estring ~loc id)
        ];
        pstr_value ~loc Nonrecursive [
          value_binding ~loc ~pat:(pvar ~loc "element") ~expr:element
        ];
        pstr_value ~loc Nonrecursive [
          value_binding ~loc ~pat:(pvar ~loc "props") ~expr:props
        ];
      ], "Vue_component.Make", `component in
  let str = str_ctype @ str_more in
  let txt = match options.name with
    | Some n -> String.capitalize_ascii n
    | None -> String.capitalize_ascii t.ptype_name.txt in
  app := Some (txt, kind, t.ptype_name.txt);
  let expr = pmod_apply ~loc (pmod_ident ~loc @@ Located.lident ~loc mod_apply) @@
    pmod_structure ~loc str in
  let m = pstr_module ~loc @@ module_binding ~loc ~name:{txt=Some txt; loc} ~expr in
  let str = str_jsoo @ [ m ] in
  if options.debug then Format.printf "%s@." @@ Pprintast.string_of_structure str;
  str

type register_options = {
  rname: string option;
  rkind: [`computed | `watch | `method_ of int];
  rasync: bool;
  rdebug: bool;
}

let empty_register_options = { rname=None; rkind=`method_ 0; rasync=false; rdebug=false }

let get_register_options l =
  let aux l = List.fold_left (fun acc ({txt; _}, e) ->
      let n = Longident.name txt in
      match n, e.pexp_desc with
      | "name", Pexp_constant (Pconst_string (s, _, _)) -> { acc with rname=Some s }
      | "computed", _ -> { acc with rkind=`computed }
      | "watch", _ -> { acc with rkind=`watch }
      | "debug", _ -> { acc with rdebug=true }
      | "async", _ -> { acc with rasync=true }
      | _ -> acc) empty_register_options l in
  List.find_map (function
      | {attr_name={txt="vue"; _}; attr_payload = PStr [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_record (l, _); _}, _) ; _} ]; _} ->
        Some (aux l)
      | {attr_name={txt="vue"; _}; _} -> Some empty_register_options
      | _ -> None) l

let register ~loc ~options vb =
  let fname = match vb.pvb_pat.ppat_desc with
    | Ppat_var {txt; _} -> txt
    | _ -> Location.raise_errorf ~loc "unexpected pattern in register function" in
  let name = Option.value ~default:fname options.rname in
  let rec aux acc e = match e.pexp_desc with
    | Pexp_fun (_, _, _, e) -> aux (acc+1) e
    | _ -> acc in
  let module_name = match !app with
    | None -> Location.raise_errorf ~loc "no module loaded"
    | Some (s, _, _) -> s in
  let n = aux 0 vb.pvb_expr in
  let kind = match options.rkind with `method_ _ -> `method_ (n-1) | x -> x in
  let async e = [%expr Lwt.async (fun () -> Lwt.map (fun _ -> ()) [%e e])] in
  let expr = match kind with
    | `computed ->
      let f =
        if options.rasync then [%expr fun app -> [%e async (eapply ~loc (evar ~loc fname) [ evar ~loc "app" ])]]
        else evar ~loc fname in
      eapply ~loc (evar ~loc (module_name ^ ".add_computed")) [
        estring ~loc name; f
      ]
    | `watch ->
      let f =
        if options.rasync then [%expr fun app -> [%e async (eapply ~loc (evar ~loc fname) [ evar ~loc "app" ])]]
        else evar ~loc fname in
      eapply ~loc (evar ~loc (module_name ^ ".add_watch")) [
        estring ~loc name; f
      ]
    | `method_ i ->
      let rec aux (acc1, acc2) i =
        if i = 0 then acc1 (async (eapply ~loc (evar ~loc fname) acc2))
        else
          aux
            ((fun e -> pexp_fun ~loc Nolabel None (pvar ~loc ("_" ^ string_of_int i)) @@ acc1 e),
             evar ~loc ("_" ^ string_of_int i) :: acc2) (i-1) in
      let f =
        if options.rasync then aux ((fun e -> e), []) n
        else evar ~loc fname in
      eapply ~loc (evar ~loc (module_name ^ ".add_method" ^ string_of_int i)) [
        estring ~loc name; f
      ] in
  let v = value_binding ~loc ~pat:(punit ~loc) ~expr in
  let str = [ pstr_value ~loc Nonrecursive [ v ] ] in
  if options.rdebug then Format.printf "%s@." @@ Pprintast.string_of_structure str;
  str

let init_extension ~loc e =
  match !app with
  | None -> Location.raise_errorf ~loc "no module loaded"
  | Some (module_name, kind, data_name) ->
    let data_name = if data_name = "t" then "to_jsoo" else data_name ^ "_to_jsoo" in
    match kind with
    | `root ->
      [%expr [%e evar ~loc (module_name ^ ".init")]
          ~data:([%e eapply ~loc (evar ~loc data_name) [e]])]
    | `component ->
      [%expr [%e evar ~loc (module_name ^ ".make")]
          ~data:(fun _ -> [%e eapply ~loc (evar ~loc data_name) [e]])]

let transform =
  object(_self)
    inherit Ast_traverse.map as super
    method! structure l =
      List.fold_left (fun acc s ->
          match s.pstr_desc with
          | Pstr_type (_, [ t ]) ->
            begin match get_module_options t.ptype_attributes with
              | None -> acc @ [ super#structure_item s ]
              | Some options ->
                let s2 = derive_module ~loc:t.ptype_loc ~options t in
                acc @ [ super#structure_item s ] @ s2
            end
          | Pstr_value (_, [ vb ]) ->
            begin match get_register_options vb.pvb_attributes with
              | None -> acc @ [ super#structure_item s ]
              | Some options ->
                let s2 = register ~loc:vb.pvb_loc ~options vb in
                acc @ [ super#structure_item s ] @ s2
            end
          | _ -> acc @ [ super#structure_item s ]) [] l
    method! expression e =
      let loc = e.pexp_loc in
      match e .pexp_desc with
      | Pexp_extension ({txt="vue"; _}, PStr [ {pstr_desc= Pstr_eval (e, _a); _} ]) ->
        init_extension ~loc @@ Ppx_deriving_jsoo_lib.Ppx_js.transform#expression e
      | _ -> super#expression e
  end

let () =
  Driver.register_transformation "vue" ~impl:transform#structure;
  let str_type_decl = Deriving.Generator.make Ppx_deriving_jsoo_lib.Ppx_deriver.args_str
      Ppx_deriving_jsoo_lib.Ppx_deriver.str_gen in
  Deriving.ignore @@ Deriving.add "jsoo" ~str_type_decl
