type computed = {
  computed_prop: int
} [@@deriving jsoo]

type cmp = {
  message: string;
} [@@vue {template="<div>{{ message }}</div>"; computed=computed_jsoo}]

let method1 app x y =
  ignore (x, y);
  Ezjs_min.js_log app##.message;
  Lwt.return_unit
[@@vue {name="method_name"; async}]

let computed_prop app =
  Ezjs_min.Optdef.option @@ int_of_string_opt @@ Ezjs_min.to_string app##.message
[@@vue {computed}]

let () =
  let _app = [%vue { message = "" } ] in
  ()
