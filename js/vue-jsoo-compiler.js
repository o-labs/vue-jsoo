#!/usr/bin/env node
const fs = require('fs')
const yargs = require('yargs')

const argv = yargs
      .scriptName("vue-jsoo-compiler")
      .usage("$0 [list of files without extension]")
      .options({
        'output': {
          alias: 'o',
          describe: 'Output file/library',
          demandOption: true,
          default: 'render',
        },
        'types': {
          describe: 'Input types module for ocaml render functions',
        },
        'vue-version': {
          alias: 'v',
          demandOption: true,
          number: true,
          default: 2
        }
      })
      .help().alias('help', 'h').argv;

if (argv.v == 2) {
  const utils = require('@vue/component-compiler-utils')
  const compiler2 = require('vue-template-compiler')

  var render_start = 'var render = function ()'
  var static_renders_start = 'var staticRenderFns'
  var s_js = ''
  var s_ml = 'open Js_of_ocaml.Js\n'
  var type_module = argv.types ? argv.types.charAt(0).toUpperCase() + argv.types.slice(1) : undefined

  for (name of argv._) {
    var tmp = name.split('[')
    var params = tmp.length > 1 ? '(_' + ',_'.repeat(Number(tmp[1].substr(0,tmp[1].length - 1)) - 1) + ') ' : ''
    var unit_sig = tmp.length > 1 ? 'unit -> ' : ''
    var unit_str = tmp.length > 1 ? 'fun () -> ' : ''
    var name = tmp[0]
    var render_type = type_module ? ': ' + unit_sig + '(' + params + type_module + '.' + name + ' t, Unsafe.any -> Unsafe.any) meth_callback ' : ''
    var static_type = type_module ? render_type + 'js_array t ' : ''
    const source = fs.readFileSync(name + '.html', 'utf8')
    let c = utils.compileTemplate({
      source,
      compiler : compiler2,
      isProduction : true,
      compilerOptions : { whitespace : 'condense' }
    })
    if (c.errors.length!=0) {
      console.error('cannot compile "' + name + '" render functions')
      for (e of c.errors) {
        console.error(e)
      }
      continue
    }
    var i_render_start = c.code.indexOf(render_start);
    var j_render_start = i_render_start = i_render_start + render_start.length
    var i_static_renders_start = c.code.indexOf(static_renders_start);
    var j_static_renders_start = i_render_start = i_static_renders_start + static_renders_start.length
    let render = c.code.slice(j_render_start, i_static_renders_start).replace(/_vm\.\$createElement/g, 'createElement')
    let static_renders = c.code.slice(j_static_renders_start).replace(/function \(\)/g, 'function(createElement)').replace(/_vm\.\$createElement/g, 'createElement')

    if (render == ' {}\n') {
      console.error('cannot compile "' + name + '" render functions')
      continue
    }

    c = '//Provides: ' + name + '_render\n' +
      'function '+ name + '_render(createElement)' +
      render +
      '\n//Provides: ' + name + '_static_renders\n' +
      'var ' + name + '_static_renders' +
      static_renders + '\n'
    s_js += c
    s_ml += 'let ' + name + '_render ' + render_type + '= ' + unit_str + 'Unsafe.pure_js_expr "' + name +'_render"\nlet ' + name + '_static_renders ' + static_type + '= ' + unit_str + 'Unsafe.pure_js_expr "' + name +'_static_renders"\n'
  }
  fs.writeFile(argv.output + '.js', s_js , function() {})
  fs.writeFile(argv.output + '.ml', s_ml , function() {})

} else {
  const babel = require('@babel/core')
  const compiler3 = require('@vue/compiler-dom')

  var render_start = 'return function render(_ctx, _cache) {\n'
  var s_js = '//Provides: Vue\nvar Vue = joo_global_object.Vue\n\n'
  var s_ml = 'open Js_of_ocaml.Js\n'
  var type_module = argv.types ? argv.types.charAt(0).toUpperCase() + argv.types.slice(1) : undefined

  let n = argv._.length

  function process(i) {
    if (i==n) {
      fs.writeFile(argv.output + '.js', s_js , function() {})
      fs.writeFile(argv.output + '.ml', s_ml , function() {})
    } else {
      var name = argv._[i]
      var tmp = name.split('[')
      var params = tmp.length > 1 ? '(_' + ',_'.repeat(Number(tmp[1].substr(0,tmp[1].length - 1)) - 1) + ') ' : ''
      var unit_sig = tmp.length > 1 ? 'unit -> ' : ''
      var unit_str = tmp.length > 1 ? 'fun () -> ' : ''
      var name = tmp[0]
      var render_type = type_module ? ': ' + unit_sig + '(' + params + type_module + '.' + name + ' t, Unsafe.any -> Unsafe.any) meth_callback ' : ''
      var static_type = type_module ? render_type + 'js_array t ' : ''
      const source = fs.readFileSync(name + '.html', 'utf8')
      let c;
      try {
        c = compiler3.compile(source, {mode:'function', whitespace:'condense', prefixIdentifiers:true})
      } catch(e) {
        console.error('cannot compile "' + name + '" render functions')
        console.error(e)
        process(i+1)
      }
      var i_render_start = c.code.indexOf(render_start);
      var j_render_start = i_render_start + render_start.length
      let render = c.code.slice(j_render_start)
      let before = c.code.slice(0, i_render_start)
      let x = 'function '+ name + '_render(_ctx, _cache) {\n  ' + before + render
      babel.transform(x, {presets:['@babel/env'], sourceType:'script'}, function(err, result) {
        if (render == ' {}') {
          console.error('cannot compile "' + name + '" render functions')
          process(i+1)
        }

        c = '//Provides: ' + name + '_render\n//Requires: Vue\n' + result.code +
          '\n//Provides: ' + name + '_static_renders\n' +
          'var ' + name + '_static_renders = []\n'
        s_js += c
        s_ml += 'let ' + name + '_render ' + render_type + '= ' + unit_str + 'Unsafe.pure_js_expr "' + name +'_render"\nlet ' + name + '_static_renders ' + static_type + '= ' + unit_str + 'Unsafe.pure_js_expr "' + name +'_static_renders"\n'
        process(i+1)
      })
    }
  }

  try {
    process(0)
  } catch(e) {
    console.error(e)
  }
}
