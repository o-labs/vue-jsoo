## [0.3](https://gitlab.com/o-labs/vue-jsoo/compare/0.2.1...0.3) (2022-12-23)

* Fix and improvements
* Vue3

## [0.2.1](https://gitlab.com/o-labs/vue-jsoo/compare/0.2...0.2.1) (2020-10-30)

* Generalize render function to allow runtime-only build

## [0.2](https://gitlab.com/o-labs/vue-jsoo/compare/0.1...0.2) (2020-09-25)

* Better Vuex Support
* Better component interface

## 0.1 (2020-07-05)

First Release
